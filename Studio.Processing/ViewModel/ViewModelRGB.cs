﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studio.Processing.ViewModel
{
    public class ViewModelRGB
    {
        public Byte Red { get; set; }
        public Byte Green { get; set; }
        public Byte Blue { get; set; }
    }
}
