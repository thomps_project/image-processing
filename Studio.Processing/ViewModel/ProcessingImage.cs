﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studio.Processing.ViewModel
{
    public class ProcessingImage
    {
        public Bitmap Bitmap { get; set; }
        public Graphics Graphics { get; set; }
    }
}
